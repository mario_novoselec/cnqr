var webJS = {
    animations: {}
};

webJS.animations = (function (window, document) {
    function homeIntro() {
        var homeMain = $('.home-main'),
            header = $('.header'),
            textTag = homeMain.find('h1'),
            charactListItem = $('.characteristics-list__item'),
            charactGroupDesc = $('.characteristics-group__description');
        new TimelineLite({
            delay: 1.2,
        })
            .add('start')
            .staggerTo(textTag, 0.4, {
                y: 0,
                autoAlpha: 1,
                ease: Quad.easeOut,
            }, 0.03, 'start')
            .set(header, {
                className: '+=line-loaded'
            })
            .staggerTo([header.find('.features__item'), header.find('.content__label')], 0.4, {
                autoAlpha: 1,
                ease: Quad.easeOut,
            }, 0.3, 'start')
            .to(header.find('.btn'), 0.5, {
                autoAlpha: 1,
                y: 0,
                ease: Power4.easeOut,
            }, '-=0.1')
            .to(charactGroupDesc, 0.4, {
                autoAlpha: 1,
            }, '-=0.3')
            .staggerTo(charactListItem, 0.4, {
                autoAlpha: 1,
            }, 0.12, '-=0.2')
    }

    function animationScrollMagic() {
        $('[data-scrollmagic]').each(function (index, elem) {
            // Init ScrollMagic Controller
            var fromAnimation = JSON.parse($(elem).attr('data-from'));
            var toAnimation = JSON.parse($(elem).attr('data-to'));
            var duration = fromAnimation.duration || toAnimation.duration || 0.6;

            var scrollMagicController = new ScrollMagic(),
                eloffset = $(elem).attr('data-offset');

            var tl = new TimelineMax({pause: true});
            tl.add("start") // add timeline label
                .fromTo($(elem), duration,
                    fromAnimation,
                    toAnimation,
                    "start");

            // Create the Scene and trigger when visible
            var scene = new ScrollScene({
                triggerElement: elem,
                triggerHook: 'onEnter',
                offset: (eloffset) ? eloffset : 50,
                reverse: false,
            })
                .setTween(tl)
                .addTo(scrollMagicController);

            // Add debug indicators fixed on right side
            // scene.addIndicators();
        });
    }

    function videoIntro() {
        var keyFeaturesBg = $('.key-features__list');
        var keyFeaturesItem = keyFeaturesBg.find('.key-features__item');
        var keyFeaturesContainer = keyFeaturesBg.find('.key-features__container');

        $('body').on('mouseover', function (e) {
            if ($(e.target).closest('.key-features').length === 0) {
                var targetBg = $('.key-features__bg-item');
                TweenMax.set(keyFeaturesItem, {
                    className: '-=item-disabled',
                })
                TweenMax.set(keyFeaturesContainer, {
                    className: '-=item-active',
                })
                new TimelineLite()
                    .set(targetBg, {
                        className: '-=hover-active',
                        clearProps: 'all',
                    })
                    .set(targetBg.find('.layer--background'), {
                        clearProps: 'all',
                    })
                    .set(targetBg, {
                        clearProps: 'all',
                    })
                    .set(targetBg.find('.key-features__video'), {
                        clearProps: 'all',
                    })
            }
        });
        keyFeaturesItem.on('mouseenter', function (e) {
            var _this = $(this);
            var itemId = _this.attr('data-id');
            var targetBg = $('[data-id="' + itemId + '"].key-features__bg-item');
            var otherItems = $('.key-features__bg-item').not(targetBg);
            var parentContainer = _this.closest('.key-features__container');

            TweenMax.set(keyFeaturesItem, {
                className: '-=item-disabled',
            })
            TweenMax.set(keyFeaturesContainer, {
                className: '-=item-active',
            })
            if (!targetBg.hasClass('hover-active')) {
                new TimelineLite({
                    onStart: function () {
                        var svg = _this.find('svg');

                        new TimelineMax()
                            .to(svg, 0.3, {x: '100%'})
                            .set(svg, {x: '-100%'})
                            .to(svg, 0.3, {x: '0%'});
                    }
                })
                    .set(keyFeaturesItem.not(_this), {
                        className: '+=item-disabled',
                    })
                    .set(parentContainer, {
                        className: '+=item-active',
                    })
                    .add('start')
                    .to(targetBg.find('.layer--background'), 0.1, {
                        autoAlpha: 0,
                        ease: Power4.easeOut,
                    }, 'start')
                    .to(targetBg, 0.5, {
                        width: '100%',
                        ease: Quad.easeInOut,
                    }, 'start')
                    .to(targetBg.find('.key-features__video'), 0.5, {
                        opacity: 0.2,
                        ease: Power4.easeOut
                    }, 'start')
                    .to(otherItems, 0.5, {
                        width: '0%',
                        autoAlpha: 0,
                        ease: Quad.easeInOut,
                    }, 'start')
                    .set($('.key-features__bg-item'), {
                        className: '+=hover-active',
                        width: '100%',
                    })
            } else {
                new TimelineLite({
                    onStart: function () {
                        var svg = _this.find('svg');

                        new TimelineMax()
                            .to(svg, 0.3, {x: '100%'})
                            .set(svg, {x: '-100%'})
                            .to(svg, 0.3, {x: '0%'});
                    }
                })
                    .set(keyFeaturesItem.not(_this), {
                        className: '+=item-disabled',
                    })
                    .set(parentContainer, {
                        className: '+=item-active',
                    })
                    .to(otherItems, 0.1, {
                        autoAlpha: 0,
                    })
                    .set(targetBg, {
                        autoAlpha: 1,
                    })
            }
        })
    }

    function navigationSlide() {
        var $menuTrigger = $('.menu__trigger.trigger');
        var $sidebar = $('.sidebar');
        var $siteContent = $('.main');

        $menuTrigger.click(function () {
            $sidebar.toggleClass('sidebar--active');
            $siteContent.toggleClass('main--active');
            $menuTrigger.toggleClass('trigger--active');
        });
    }

    return {
        homeIntro: homeIntro,
        animationScrollMagic: animationScrollMagic,
        videoIntro: videoIntro,
        navigationSlide: navigationSlide
    };

})(window, document);

var videoModal = (function () {
    function init() {
        var modal = $('.video-modal');
        var video = $('.video-modal-wrapper video').get(0);

        $('.video-wrapper video').click(function () {
            modal.addClass('video-modal--open');

            TweenLite.to(modal, 0.3, {
                opacity: 1,
                ease: Power1.easeInOut,
                onComplete: function () {
                    video.play();
                },
            });

        });

        $('.close-video-modal').click(function () {
            video.pause();
            TweenLite.to(modal, 0.3, {
                opacity: 0,
                ease: Power1.easeInOut,
                onComplete: function () {
                    video.currentTime = 0;
                    modal.removeClass('video-modal--open');
                }
            });
        });
    }

    return {
        init: init
    }
})();

var buttonAnimation = (function () {
    function init() {
        $('.btn').mouseover(function () {
            var svg = $(this).find('svg');
            new TimelineMax()
                .to(svg, 0.3, {x: '100%'})
                .set(svg, {x: '-100%'})
                .to(svg, 0.3, {x: '0%'});
        });
    }

    return {
        init: init
    }
})();

var articleHover = (function () {
    function init() {
        var $articleWrapper = $('.article-img-wrapper');

        $articleWrapper.mouseover(function () {
            var border = $(this).find('.article-img__border');
            var arrow = $(this).find('.article-img__corner__wrapper');
            var img = $(this).find('.article-img');

            new TimelineLite()
                .to(border, 0.35, {opacity: 1}, 0)
                .to(arrow, 0.5, {x: 0}, 0.1)
                .to(img, 1, {scale: 1.1, ease: Power1.easeOut}, 0.1)
        });

        $articleWrapper.mouseout(function () {
            var border = $(this).find('.article-img__border');
            var arrow = $(this).find('.article-img__corner__wrapper');
            var img = $(this).find('.article-img');

            new TimelineLite()
                .to(img, 0.5, {scale: 1, ease: Power1.easeOut}, 0)
                .to(arrow, 0.3, {x: -arrow.width()}, 0)
                .to(border, 0.35, {opacity: 0}, 0.1);
        });
    }

    return {
        init: init
    }
})();


$(function () {
    //$.fn.gridSys();
    webJS.animations.animationScrollMagic();
    webJS.animations.homeIntro();
    webJS.animations.navigationSlide();
    if ($(window).width() >= 1050) {
        webJS.animations.videoIntro();
    }

    videoModal.init();
    buttonAnimation.init();
    articleHover.init();
    $('input[type="text"], textarea').on('keyup', function(){
        var val = $(this).val();
        if(val !== ""){
            $(this).next().css('display', 'none');
        }else{
            $(this).next().css('display', 'block');
        }
    })
    var w = $(window);
    $(window).on('scroll', function(){
        var nav = $('.navigation');
        console.log(nav.offset().top)
        if(w.scrollTop() > 0){
            nav.addClass('border-hidden');
        }else{
            nav.removeClass('border-hidden');
        }
    })
});



