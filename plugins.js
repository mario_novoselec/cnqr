module.exports = {
	scripts: [
		// PLUGINS
		'static/js/vendor/jquery.js',
		'static/js/vendor/is.js',
		'static/js/vendor/TweenMax.js',
		'static/js/vendor/jquery.scrollmagic.js',
		// CUSTOM
		'static/js/custom/main.js'
	]
};